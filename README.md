# Praca Dyplomowa

Praca inżynierska - Adrian Winkler. Projekt zakładał stworzenie ramienia robota 5-osiowego w konfiguracji antropomorficznej wraz z chwytakiem. Model stanowił w pełni autorski pomysł. Całość projektowano z myślą wydrukowania na drukarce 3D. Napęd poszczególnych złącz stanowiły silniki krokowe. Chwytak napędzany był z pomocą niewielkiego serwomechanizmu. Część sterującą stanowiły dwa mikrokontrolery komunikujące się po magistrali I2C. Pierwszy - STM, był urządzeniem nadrzędnym, natomiast drugi - AVR, odpowiadał za obsługę wyświetlacza. Całość dopełniał swoisty panel sterujący, pozwalający na ręczne sterowanie urządzeniem, przeprowadzanie bazowania, a także uruchamianie trybu demonstracyjnego, w którym to robot realizuje zapisaną sekwencję ruchów. 

# Działanie robota

Link do nagrania ukazującego sposób działania robota poniżej:

https://drive.google.com/file/d/1-M1aiHCL8LkhtLV4VS7dzW92MtGJBxhj/view

## Modele z zewnętrznych źródeł

Nie wszystkie elementy zaprojektowano osobiście. Poniżej lista modeli z zewnętrznych źródeł:

- Przekładnia planetarna - https://www.thingiverse.com/thing:931555
- Przekładnia ślimakowa - https://www.thingiverse.com/thing:2018837
- Przekładnia kątowa - https://grabcad.com/library/bevel-gear-28
- Chwytak - https://grabcad.com/library/gripper-210 
- Koła satelitarne przekładni planetarnej - https://www.thingiverse.com/thing:931555
- Model silnika Nema 17 - https://grabcad.com/library/nema-17-42a02c-aliexpress-1
- Koła zębate GT2: - https://grabcad.com/library/gt2-16t-pulley-1 oraz
 https://grabcad.com/library/gt2-60t-5mm-bore-1
- Serwomotor Tower G90: https://grabcad.com/library/sg90-servomotor-1

Wszystkie powyższe linki pozwalały na dostęp w dniu 12.12.21 r.

## Obrazy wykorzystane w panelu operatorskim i logo AGH:

- Tło: https://images.wallpapersden.com/image/wxl-line-shadow-background_2209.jpg
- Symbol kontrolera: https://t4.ftcdn.net/jpg/03/00/99/31/360_F_300993116_olBdarmuH4rrAdOhfzLFmaI5T3cz3TYi.webp
- Symbol domu: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsQmK3KF-pZeH9ntMqv9A-uPiRzRkEKHW9Lw&usqp=CAU
- Logo AGH: https://www.agh.edu.pl/uczelnia/system-identyfikacji-wizualnej/znakgraficzny-agh/wersje-barwne-znaku/

Wszystkie powyższe linki pozwalały na dostęp w dniu 12.12.21 r.
